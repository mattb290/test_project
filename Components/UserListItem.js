import React, { PureComponent } from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';

export default class UserListItem extends PureComponent {
  constructor(props){
    super(props)

    this.onButtonPress = this.onButtonPress.bind(this)
  }

  // Handle button press - reveal users name
  onButtonPress() {
    Alert.alert('I am '+this.props.item.name)
  }

  render() {
    const { item } = this.props
    return (
      <View style={styles.userListItem}>
        <View style={{justifyContent: 'center'}}>
          <Text style={{fontSize:17, fontWeight:'bold'}}>{`${item.email}`}</Text>
          <Text>{`ID: ${item.id}`}</Text>
        </View>
        <View style={{height:20, marginTop:25}}>
          <Button
            onPress={this.onButtonPress}
            title="Who am I?"
            color="#841584"
            accessibilityLabel="Displays users name"
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  userListItem: {
    flex: 1,
    justifyContent:'space-between',
    height: 100,
    marginTop:10,
    marginBottom:10,
    padding:5,
    flexDirection: 'row',
    borderRadius: 4,
    borderWidth:1,
    borderColor:'gray',
    backgroundColor:'#edeef4'
  }
})
