import React, { Component } from 'react';
import { FlatList, Alert } from 'react-native';
import UserListItem from './UserListItem'

export default class UserList extends Component {
  constructor(props){
    super(props)

    this.state = {
      userList : [] // List of users to render
    }

    this.loadMoreUsers = this.loadMoreUsers.bind(this)
  }

  componentWillMount() {
    this.loadMoreUsers()
  }

  // Async request to retrieve user data
  async loadMoreUsers() {
    try {
      let response = await fetch('https://jsonplaceholder.typicode.com/users')
      let newUsers = await response.json()
      this.setState({ userList: [...this.state.userList, ...newUsers] })
    } catch(err){
      Alert.alert('Something went wrong!', err.toString())
    }
  }

  render() {
    return (
      <FlatList
        data={this.state.userList}
        keyExtractor={(item, index) => index} // Don't do keys like this
        renderItem={({item}) => <UserListItem item={item}/> }
        onEndReachedThreshold={0.2}
        onEndReached={this.loadMoreUsers}
      />
    );
  }
}
