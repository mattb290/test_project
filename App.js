import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import UserList from './Components/UserList'

export default class AppMain extends Component {
  render() {
    return (
      <View style={styles.appContainer}>
        <UserList />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
    backgroundColor: '#f7feff',
    alignItems: 'center'
  }
})
